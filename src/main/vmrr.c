/**
 * @file:	vmrr.c
 * @brief:	main() for Virtual Model Railroad.
 * 
 * @copyright:	Copyright (C) 2020 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * System libraries
 */
#include <config.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * GNU portability library (Gnulib)
 */
   
/**
 * Third-party libraries
 */
  
/**
 * VMRR code
 */
#include "vmrr.h"
 
/**
 * main()
 */
int main (int argc, char *argv[]) {
  int result =0;
  puts ("Virtual Model Railroad");
  puts ("This is " PACKAGE_STRING ".");
  exit(result);
}