# README #

The Virtual Model Railroad (VMRR) is an exercise in thread synchronization and
interprocess communications. 

We begin with a single command line process and refactor as we advance.

A physical railroad, model or otherwise, is a nodal network. We establish a 
rail line as a process along which locomotives (threads) will "travel". We 
allow rolling stock to be linked to a locomotive and load and off-load cargo 
and passengers along the way. Thread synchronization objects will be used to 
control locomotive traffic on the line.

## COPYRIGHT ##

Copyright (C) 2020 Kuhrman Technology Solutions LLC

LICENSE: GPLv3+: GNU GPL version 3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is included in this package in 
the file named COPYING. Otherwise see <http://www.gnu.org/licenses/>.

## INSTALLATION ##

This is a Gnu Autotools project. If you are new to Autotools or otherwise 
need an easy place to begin, see: 
<https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html>

Machine-dependent files and Autotools-generated files are not currently maintained 
under source control for this project. If you cloned this project from a Git repository
it may be necessary to run the following before you can build the project.

    [user@example.com ~/virtual-model-railroad]$ aclocal
    [user@example.com ~/virtual-model-railroad]$ autoreconf --install
    
Generic Autotools instructions for building the project are included in the file 
named INSTALL.

## COMMUNICATION ##

Send bug reports and other project communication to <karl@kuhrman.net>.